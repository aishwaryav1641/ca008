#include<stdio.h>

int main()
{
    struct complex
    {
        float re;
        float im;
    };
    struct complex c1,c2,c3,c4;
    printf("enter the real and imaginary parts of the first complex number\t");
    scanf("%f %f",&c1.re,&c1.im);
    printf("enter the real and imaginary parts of the second complex number\t");
    scanf("%f %f",&c2.re,&c2.im);
    c3.re=c1.re+c2.re;
    c3.im=c1.im+c2.im;
    c4.re=c1.re-c2.re;
    c4.im=c1.im-c2.im;
    printf("addition\t");
    printf("%f+i%f\n",c3.re,c3.im);
    printf("subtraction\t");
    printf("%f+i%f",c4.re,c4.im);
    return 0;
}
